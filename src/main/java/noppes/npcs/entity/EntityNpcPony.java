package noppes.npcs.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import noppes.npcs.ModelData;

public class EntityNpcPony extends EntityNPCInterface {
  public boolean isPegasus = false;
  
  public boolean isUnicorn = false;
  
  public boolean isFlying = false;
  
  public boolean isMale = false;
  
  public ResourceLocation checked = null;
  
  public EntityNpcPony(World world) {
    super(world);
    this.display.texture = "customnpcs:textures/entity/ponies/Anon.png";
  }
  
  public void onUpdate() {
    this.isDead = true;
    if (!this.worldObj.isRemote) {
      NBTTagCompound compound = new NBTTagCompound();
      writeToNBT(compound);
      EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
      npc.readFromNBT(compound);
      ModelData data = npc.modelData;
      data.setEntityClass(EntityNpcPony.class);
      this.worldObj.spawnEntityInWorld((Entity)npc);
    } 
    super.onUpdate();
  }

@Override
public void addChatMessage(IChatComponent p_145747_1_) {
	// TODO Auto-generated method stub
	
}

@Override
public boolean canCommandSenderUseCommand(int p_70003_1_, String p_70003_2_) {
	// TODO Auto-generated method stub
	return false;
}

@Override
public ChunkCoordinates getPlayerCoordinates() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public World getEntityWorld() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void attackEntityWithRangedAttack(EntityLivingBase p_82196_1_, float p_82196_2_) {
	// TODO Auto-generated method stub
	
}
}