package noppes.npcs.client.model.util;

import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.PositionTextureVertex;
import net.minecraft.client.model.TexturedQuad;
import net.minecraft.client.renderer.Tessellator;
import noppes.npcs.client.renderer.EnumPlanePosition;

public class ModelPlane extends ModelBox {

   private PositionTextureVertex[] vertexPositions = new PositionTextureVertex[8];
   private TexturedQuad quad;


   public ModelPlane(ModelRenderer par1ModelRenderer, int textureOffsetX, int textureOffsetY, float x, float y, float z, int dx, int dy, int dz, float scale, EnumPlanePosition position, boolean mirrorxy) {
      super(par1ModelRenderer, textureOffsetX, textureOffsetY, x, y, z, dx, dy, dz, scale);
      
      float X_DX = x + (float)dx;
      float Y_DY = y + (float)dy;
      float Z_DZ = z + (float)dz;
      
      x -= scale;
      y -= scale;
      z -= scale;
      
      X_DX += scale;
      Y_DY += scale;
      Z_DZ += scale;
      
      if(par1ModelRenderer.mirror) { //x mirror
         float DX_X = X_DX;
         X_DX = x;
         x = DX_X;
      }

      if(mirrorxy) {
    	  float DZ_Z = Z_DZ;
    	  Z_DZ = z;
          z = DZ_Z;
          
    	  float DX_X = X_DX;
          X_DX = x;
          x = DX_X;
      }
      
      PositionTextureVertex var231 = new PositionTextureVertex(x, y, z, 0.0F, 0.0F);
      PositionTextureVertex var15 = new PositionTextureVertex(X_DX, y, z, 0.0F, 8.0F);
      PositionTextureVertex var16 = new PositionTextureVertex(X_DX, Y_DY, z, 8.0F, 8.0F);
      PositionTextureVertex var17 = new PositionTextureVertex(x, Y_DY, z, 8.0F, 0.0F);
      PositionTextureVertex var18 = new PositionTextureVertex(x, y, Z_DZ, 0.0F, 0.0F);
      PositionTextureVertex var19 = new PositionTextureVertex(X_DX, y, Z_DZ, 0.0F, 8.0F);
      PositionTextureVertex var20 = new PositionTextureVertex(X_DX, Y_DY, Z_DZ, 8.0F, 8.0F);
      PositionTextureVertex var21 = new PositionTextureVertex(x, Y_DY, Z_DZ, 8.0F, 0.0F);
      this.vertexPositions[0] = var231;
      this.vertexPositions[1] = var15;
      this.vertexPositions[2] = var16;
      this.vertexPositions[3] = var17;
      this.vertexPositions[4] = var18;
      this.vertexPositions[5] = var19;
      this.vertexPositions[6] = var20;
      this.vertexPositions[7] = var21;
      if(position == EnumPlanePosition.LEFT) {
         this.quad = new TexturedQuad(new PositionTextureVertex[]{var19, var15, var16, var20}, textureOffsetX, textureOffsetY, textureOffsetX + dz, textureOffsetY + dy, par1ModelRenderer.textureWidth, par1ModelRenderer.textureHeight);
      }

      if(position == EnumPlanePosition.TOP) {
         this.quad = new TexturedQuad(new PositionTextureVertex[]{var19, var18, var231, var15}, textureOffsetX, textureOffsetY, textureOffsetX + dx, textureOffsetY + dz, par1ModelRenderer.textureWidth, par1ModelRenderer.textureHeight);
      }

      if(position == EnumPlanePosition.BACK) {
         this.quad = new TexturedQuad(new PositionTextureVertex[]{var15, var231, var17, var16}, textureOffsetX, textureOffsetY, textureOffsetX + dx, textureOffsetY + dy, par1ModelRenderer.textureWidth, par1ModelRenderer.textureHeight);
      }

      if(par1ModelRenderer.mirror) {
         this.quad.flipFace();
      }

   }

   public void render(Tessellator par1Tessellator, float par2) {
      this.quad.draw(par1Tessellator, par2);
   }
}
