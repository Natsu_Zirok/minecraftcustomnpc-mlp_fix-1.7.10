package noppes.npcs.client.model.util;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import noppes.npcs.client.model.util.ModelPlane;
import noppes.npcs.client.renderer.EnumPlanePosition;

public class ModelPlaneRenderer extends ModelRenderer {

   private int textureOffsetX;
   private int textureOffsetY;

   public boolean mirrorxy = false;

   public ModelPlaneRenderer(ModelBase modelbase, int i, int j) {
      super(modelbase, i, j);
      this.textureOffsetX = i;
      this.textureOffsetY = j;
   }

   public void addBackPlane(float f, float f1, float f2, int i, int j) {
      this.addPlane(f, f1, f2, i, j, 0, 0.0F, EnumPlanePosition.BACK);
   }

   public void addSidePlane(float f, float f1, float f2, int j, int k) {
      this.addPlane(f, f1, f2, 0, j, k, 0.0F, EnumPlanePosition.LEFT);
   }

   public void addTopPlane(float f, float f1, float f2, int i, int k) {
      this.addPlane(f, f1, f2, i, 0, k, 0.0F, EnumPlanePosition.TOP);
   }

   public void addBackPlane(float f, float f1, float f2, int i, int j, float scale) {
      this.addPlane(f, f1, f2, i, j, 0, scale, EnumPlanePosition.BACK);
   }

   public void addSidePlane(float f, float f1, float f2, int j, int k, float scale) {
      this.addPlane(f, f1, f2, 0, j, k, scale, EnumPlanePosition.LEFT);
   }

   public void addTopPlane(float f, float f1, float f2, int i, int k, float scale) {
      this.addPlane(f, f1, f2, i, 0, k, scale, EnumPlanePosition.TOP);
   }

   public void addPlane(float x, float y, float z, int dx, int dy, int dz, float scale, EnumPlanePosition pos) {
      super.cubeList.add(new ModelPlane(this, this.textureOffsetX, this.textureOffsetY, x, y, z, dx, dy, dz, scale, pos, this.mirrorxy));
   }
}
