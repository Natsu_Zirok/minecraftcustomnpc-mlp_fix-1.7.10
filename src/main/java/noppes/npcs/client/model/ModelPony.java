package noppes.npcs.client.model;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import noppes.npcs.client.model.util.ModelPlaneRenderer;
import noppes.npcs.constants.EnumAnimation;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.entity.EntityNpcPony;
import org.lwjgl.opengl.GL11;

public class ModelPony extends ModelBase {
  private boolean rainboom;

  private float WingRotateAngleX;

  private float WingRotateAngleY;

  private float WingRotateAngleZ;

  private float TailRotateAngleY;
  
  public ModelRenderer Head;
  
  public ModelPlaneRenderer[] MuzzleFemale;
  
  public ModelPlaneRenderer[] MuzzleMale;

  public ModelRenderer[] Headpiece;

  public ModelRenderer Helmet;

  public ModelRenderer Body;

  public ModelPlaneRenderer[] BodypieceNeck;
  
  public ModelPlaneRenderer[] Bodypiece;

  public ModelRenderer RightArm;

  public ModelRenderer LeftArm;

  public ModelRenderer RightLeg;

  public ModelRenderer LeftLeg;

  public ModelRenderer unicornarm;

  public ModelPlaneRenderer[] Tail;

  public ModelRenderer[] LeftWing;

  public ModelRenderer[] RightWing;

  public ModelRenderer[] LeftWingExt;

  public ModelRenderer[] RightWingExt;

  public boolean isPegasus;

  public boolean isUnicorn;

  public boolean isFlying;

  public boolean isGlow;

  public boolean isSleeping;

  public boolean isSneak;

  public boolean aimedBow;

  public boolean isMale;
  
  public int heldItemRight;

  public ModelPony(float f) {
    init(f, 0.0F);
  }

  public void init(float strech, float f) {
    initPHead(strech, f);
    initPBody(strech, f);
    initPLegs(strech, f);
    initPTail(strech, f);
    initPWings(strech, f);
  }

  public void initPHead(float strech, float f) {
    this.Head = new ModelRenderer(this, 0, 0);
    this.Head.addBox(-4.0F, -5.0F, -6.0F, 8, 8, 8, strech);
    this.Head.setRotationPoint(0.0F, f, 0.0F);

    initPHeadpiece(strech, f);
    initPMuzzle(strech, f);
    initPHelmet(strech, f);
  }
  
  public void initPMuzzle(float strech, float f) {
	  this.MuzzleFemale = new ModelPlaneRenderer[10];
	  
	  this.MuzzleFemale[0] = new ModelPlaneRenderer(this, 10, 14);
	  this.MuzzleFemale[0].addBackPlane(-2.0F, 1.0F, -7.0F +0.01F, 4, 2, strech);
	  this.MuzzleFemale[0].setRotationPoint(0.0F, f, 0.0F);
	    
	  this.MuzzleFemale[1] = new ModelPlaneRenderer(this, 11, 13);
	  this.MuzzleFemale[1].addBackPlane(-1.0F, 0.0F, -7.0F +0.01F, 2, 1, strech);
	  this.MuzzleFemale[1].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[2] = new ModelPlaneRenderer(this, 9, 14);
	  this.MuzzleFemale[2].addTopPlane(-2.0F, 1.0F, -7.0F +0.01F, 1, 1, strech);
	  this.MuzzleFemale[2].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[3] = new ModelPlaneRenderer(this, 14, 14);
	  this.MuzzleFemale[3].addTopPlane(1.0F, 1.0F, -7.0F +0.01F, 1, 1, strech);
	  this.MuzzleFemale[3].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[4] = new ModelPlaneRenderer(this, 11, 12);
	  this.MuzzleFemale[4].addTopPlane(-1.0F, 0.0F, -7.0F +0.01F, 2, 1, strech);
	  this.MuzzleFemale[4].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[5] = new ModelPlaneRenderer(this, 18, 7);
	  this.MuzzleFemale[5].addTopPlane(-2.0F, 3.0F, -7.0F +0.01F, 4, 1, strech);
	  this.MuzzleFemale[5].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[6] = new ModelPlaneRenderer(this, 9, 14);
	  this.MuzzleFemale[6].addSidePlane(-2.0F, 1.0F, -7.0F +0.01F, 2, 1, strech);
	  this.MuzzleFemale[6].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[7] = new ModelPlaneRenderer(this, 14, 14);
	  this.MuzzleFemale[7].addSidePlane(2.0F, 1.0F, -7.0F +0.01F, 2, 1, strech);
	  this.MuzzleFemale[7].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[8] = new ModelPlaneRenderer(this, 11, 12);
	  this.MuzzleFemale[8].addSidePlane(-1.0F, 0.0F, -7.0F +0.01F, 1, 1, strech);
	  this.MuzzleFemale[8].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleFemale[9] = new ModelPlaneRenderer(this, 12, 12);
	  this.MuzzleFemale[9].addSidePlane(1.0F, 0.0F, -7.0F +0.01F, 1, 1, strech);
	  this.MuzzleFemale[9].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleMale = new ModelPlaneRenderer[5];
	  
	  this.MuzzleMale[0] = new ModelPlaneRenderer(this, 10, 13);
	  this.MuzzleMale[0].addBackPlane(-2.0F, 0.0F, -7.0F +0.01F, 4, 3, strech);
	  this.MuzzleMale[0].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleMale[1] = new ModelPlaneRenderer(this, 10, 13);
	  this.MuzzleMale[1].addTopPlane(-2.0F, 0.0F, -7.0F +0.01F, 4, 1, strech);
	  this.MuzzleMale[1].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleMale[2] = new ModelPlaneRenderer(this, 18, 7);
	  this.MuzzleMale[2].addTopPlane(-2.0F, 3.0F, -7.0F +0.01F, 4, 1, strech);
	  this.MuzzleMale[2].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleMale[3] = new ModelPlaneRenderer(this, 10, 13);
	  this.MuzzleMale[3].addSidePlane(-2.0F, 0.0F, -7.0F +0.01F, 3, 1, strech);
	  this.MuzzleMale[3].setRotationPoint(0.0F, f, 0.0F);
	  
	  this.MuzzleMale[4] = new ModelPlaneRenderer(this, 13, 13);
	  this.MuzzleMale[4].addSidePlane(2.0F, 0.0F, -7.0F +0.01F, 3, 1, strech);
	  this.MuzzleMale[4].setRotationPoint(0.0F, f, 0.0F); 
  }
  
  public void initPHeadpiece(float strech, float f) {
    this.Headpiece = new ModelRenderer[3];

    this.Headpiece[0] = new ModelRenderer(this, 12, 16);
    this.Headpiece[0].addBox(-4.0F, -7.0F, -1.0F, 2, 2, 2, strech);
    this.Headpiece[0].setRotationPoint(0.0F, f, 0.0F);
    this.Headpiece[1] = new ModelRenderer(this, 12, 16);
    this.Headpiece[1].addBox(2.0F, -7.0F, -1.0F, 2, 2, 2, strech);
    this.Headpiece[1].setRotationPoint(0.0F, f, 0.0F);
    this.Headpiece[2] = new ModelRenderer(this, 0, 3);
    this.Headpiece[2].addBox(-0.5F, -11.0F, -3.5F, 1, 4, 1, strech);
    this.Headpiece[2].setRotationPoint(0.0F, f, 0.0F);
  }

  public void initPHelmet(float strech, float f) {
    this.Helmet = new ModelRenderer(this, 32, 0);
    this.Helmet.addBox(-4.0F, -5.0F, -6.0F, 8, 8, 8, strech + 0.5F);
    this.Helmet.setRotationPoint(0.0F, 0.0F, 0.0F);
  }
    
  public void initPBody(float strech, float f) {
    this.Body = new ModelRenderer(this, 16, 16);
    this.Body.addBox(-4.0F, 4.0F, -2.0F, 8, 8, 4, strech);
    this.Body.setRotationPoint(0.0F, f, 0.0F);

    initPBodypieceNeck(strech, f);
    initPBodypiece(strech, f);
  }

  public void initPBodypieceNeck(float strech, float f) {
    this.BodypieceNeck = new ModelPlaneRenderer[4];

    this.BodypieceNeck[0] = new ModelPlaneRenderer(this, 0, 16);
    this.BodypieceNeck[0].addBackPlane(-2.0F, -6.8F + 8.0F, -8.8F + 6.0F, 4, 4, strech);
    this.BodypieceNeck[0].setRotationPoint(0.0F, f, 0.0F);
    this.BodypieceNeck[1] = new ModelPlaneRenderer(this, 0, 16);
    this.BodypieceNeck[1].addBackPlane(-2.0F, -6.8F + 8.0F, -4.8F + 6.0F, 4, 4, strech);
    this.BodypieceNeck[1].setRotationPoint(0.0F, f, 0.0F);
    this.BodypieceNeck[2] = new ModelPlaneRenderer(this, 0, 16);
    this.BodypieceNeck[2].addSidePlane(-2.0F, -6.8F + 8.0F, -8.8F + 6.0F, 4, 4, strech);
    this.BodypieceNeck[2].setRotationPoint(0.0F, f, 0.0F);
    this.BodypieceNeck[3] = new ModelPlaneRenderer(this, 0, 16);
    this.BodypieceNeck[3].addSidePlane(2.0F, -6.8F + 8.0F, -8.8F + 6.0F, 4, 4, strech);
    this.BodypieceNeck[3].setRotationPoint(0.0F, f, 0.0F);
  }

  public void initPBodypiece(float strech, float f) {
    this.Bodypiece = new ModelPlaneRenderer[14];

    this.Bodypiece[0] = new ModelPlaneRenderer(this, 24, 0);
    this.Bodypiece[0].addSidePlane(-4.0F, 4.0F, 2.0F, 8, 8, strech);
    this.Bodypiece[0].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[1] = new ModelPlaneRenderer(this, 24, 0);
    this.Bodypiece[1].addSidePlane(4.0F, 4.0F, 2.0F, 8, 8, strech);
    this.Bodypiece[1].setRotationPoint(0.0F, f, 0.0F);
    
    this.Bodypiece[2] = new ModelPlaneRenderer(this, 32, 20);
    this.Bodypiece[2].mirrorxy = true;
    this.Bodypiece[2].addTopPlane(-4.0F, 4.0F, 2.0F, 8, 12, strech);
    this.Bodypiece[2].setRotationPoint(0.0F, f, 0.0F);
    

    this.Bodypiece[3] = new ModelPlaneRenderer(this, 56, 0);
    this.Bodypiece[3].addTopPlane(-4.0F, 12.0F, 2.0F, 8, 8, strech);
    this.Bodypiece[3].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[4] = new ModelPlaneRenderer(this, 4, 0);
    this.Bodypiece[4].addSidePlane(-4.0F, 4.0F, 10.0F, 8, 4, strech);
    this.Bodypiece[4].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[5] = new ModelPlaneRenderer(this, 4, 0);
    this.Bodypiece[5].addSidePlane(4.0F, 4.0F, 10.0F, 8, 4, strech);
    this.Bodypiece[5].setRotationPoint(0.0F, f, 0.0F);

    this.Bodypiece[6] = new ModelPlaneRenderer(this, 36, 16);
    this.Bodypiece[6].addBackPlane(-4.0F, 4.0F, 14.0F, 8, 4, strech);
    this.Bodypiece[6].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[7] = new ModelPlaneRenderer(this, 36, 16);
    this.Bodypiece[7].addBackPlane(-4.0F, 8.0F, 14.0F, 8, 4, strech);
    this.Bodypiece[7].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[8] = new ModelPlaneRenderer(this, 36, 16);
    this.Bodypiece[8].addTopPlane(-4.0F, 12.0F, 10.0F, 8, 4, strech);
    this.Bodypiece[8].setRotationPoint(0.0F, f, 0.0F);

    this.Bodypiece[9] = new ModelPlaneRenderer(this, 32, 0);
    this.Bodypiece[9].addTopPlane(-1.0F, 10.0F, 8.0F, 2, 6, strech);
    this.Bodypiece[9].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[10] = new ModelPlaneRenderer(this, 32, 0);
    this.Bodypiece[10].addTopPlane(-1.0F, 12.0F, 8.0F, 2, 6, strech);
    this.Bodypiece[10].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[11] = new ModelPlaneRenderer(this, 32, 0);
    this.Bodypiece[11].addSidePlane(-1.0F, 10.0F, 8.0F, 2, 6, strech);
    this.Bodypiece[11].mirror = true;
    
    this.Bodypiece[11].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[12] = new ModelPlaneRenderer(this, 32, 0);
    this.Bodypiece[12].addSidePlane(1.0F, 10.0F, 8.0F, 2, 6, strech);
    this.Bodypiece[12].setRotationPoint(0.0F, f, 0.0F);
    this.Bodypiece[13] = new ModelPlaneRenderer(this, 32, 0);
    this.Bodypiece[13].addBackPlane(-1.0F, 10.0F, 14.0F, 2, 2, strech);
    this.Bodypiece[13].setRotationPoint(0.0F, f, 0.0F);
  }
    
  public void initPLegs(float strech, float f) {
    this.RightArm = new ModelRenderer(this, 40, 16);
    this.RightArm.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.RightArm.setRotationPoint(-3.0F, 8.0F + f, 0.0F);
    
    this.LeftArm = new ModelRenderer(this, 40, 16);
    this.LeftArm.mirror = true;
    this.LeftArm.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.LeftArm.setRotationPoint(3.0F, 8.0F + f, 0.0F);

    this.RightLeg = new ModelRenderer(this, 0, 16);
    this.RightLeg.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.RightLeg.setRotationPoint(-3.0F, 0.0F + f, 0.0F);
    
    this.LeftLeg = new ModelRenderer(this, 0, 16);
    this.LeftLeg.mirror = true;
    this.LeftLeg.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.LeftLeg.setRotationPoint(3.0F, 0.0F + f, 0.0F);

    this.unicornarm = new ModelRenderer(this, 40, 16);
    this.unicornarm.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, strech);
    this.unicornarm.setRotationPoint(-5.0F, 2.0F + f, 0.0F);
  }

  public void initPTail(float strech, float f) {
    this.Tail = new ModelPlaneRenderer[21];
    
    this.Tail[0] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[0].addTopPlane(-2.0F, 1.0F, 2.0F, 4, 4, strech);
    this.Tail[0].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[1] = new ModelPlaneRenderer(this, 36, 0);
    this.Tail[1].addSidePlane(-2.0F, 1.0F, 2.0F, 4, 4, strech);
    this.Tail[1].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[2] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[2].addBackPlane(-2.0F, 1.0F, 2.0F, 4, 4, strech);
    this.Tail[2].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    
    this.Tail[3] = new ModelPlaneRenderer(this, 36, 0);
    this.Tail[3].addSidePlane(2.0F, 1.0F, 2.0F, 4, 4, strech);
    this.Tail[3].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[4] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[4].addBackPlane(-2.0F, 1.0F, 6.0F, 4, 4, strech);
    this.Tail[4].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    
    this.Tail[5] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[5].addTopPlane(-2.0F, 5.0F, 2.0F, 4, 4, strech);
    this.Tail[5].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[6] = new ModelPlaneRenderer(this, 36, 4);
    this.Tail[6].addSidePlane(-2.0F, 5.0F, 2.0F, 4, 4, strech);
    this.Tail[6].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[7] = new ModelPlaneRenderer(this, 32, 4);
    this.Tail[7].addBackPlane(-2.0F, 5.0F, 2.0F, 4, 4, strech);
    this.Tail[7].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[8] = new ModelPlaneRenderer(this, 36, 4);
    this.Tail[8].addSidePlane(2.0F, 5.0F, 2.0F, 4, 4, strech);
    this.Tail[8].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    
    this.Tail[9] = new ModelPlaneRenderer(this, 32, 4);
    this.Tail[9].addBackPlane(-2.0F, 5.0F, 6.0F, 4, 4, strech);
    this.Tail[9].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[10] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[10].addTopPlane(-2.0F, 9.0F, 2.0F, 4, 4, strech);
    this.Tail[10].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[11] = new ModelPlaneRenderer(this, 36, 0);
    this.Tail[11].addSidePlane(-2.0F, 9.0F, 2.0F, 4, 4, strech);
    this.Tail[11].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[12] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[12].addBackPlane(-2.0F, 9.0F, 2.0F, 4, 4, strech);
    this.Tail[12].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[13] = new ModelPlaneRenderer(this, 36, 0);
    this.Tail[13].addSidePlane(2.0F, 9.0F, 2.0F, 4, 4, strech);
    this.Tail[13].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[14] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[14].addBackPlane(-2.0F, 9.0F, 6.0F, 4, 4, strech);
    this.Tail[14].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[15] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[15].addTopPlane(-2.0F, 13.0F, 2.0F, 4, 4, strech);
    this.Tail[15].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[16] = new ModelPlaneRenderer(this, 36, 4);
    this.Tail[16].addSidePlane(-2.0F, 13.0F, 2.0F, 4, 4, strech);
    this.Tail[16].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.Tail[17] = new ModelPlaneRenderer(this, 32, 4);
    this.Tail[17].addBackPlane(-2.0F, 13.0F, 2.0F, 4, 4, strech);
    this.Tail[17].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[18] = new ModelPlaneRenderer(this, 36, 4);
    this.Tail[18].addSidePlane(2.0F, 13.0F, 2.0F, 4, 4, strech);
    this.Tail[18].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    
    this.Tail[19] = new ModelPlaneRenderer(this, 32, 4);
    this.Tail[19].addBackPlane(-2.0F, 13.0F, 6.0F, 4, 4, strech);
    this.Tail[19].setRotationPoint(0.0F, 0.8F + f, 0.0F);
    this.Tail[20] = new ModelPlaneRenderer(this, 32, 0);
    this.Tail[20].addTopPlane(-2.0F, 17.0F, 2.0F, 4, 4, strech);
    this.Tail[20].setRotationPoint(0.0F, 0.8F + f, 0.0F);

    this.TailRotateAngleY=this.Tail[0].rotateAngleY;
    this.TailRotateAngleY=this.Tail[0].rotateAngleY;
  }

  public void initPWings(float strech, float f) {
    this.LeftWing = new ModelRenderer[3];

    this.LeftWing[0] = new ModelRenderer(this, 56, 16);
    this.LeftWing[0].mirror = true;
    this.LeftWing[0].addBox(4.0F, 5.0F, 2.0F, 2, 6, 2, strech);
    this.LeftWing[0].setRotationPoint(0.0F, f, 0.0F);
    this.LeftWing[1] = new ModelRenderer(this, 56, 16);
    this.LeftWing[1].mirror = true;
    this.LeftWing[1].addBox(4.0F, 5.0F, 4.0F, 2, 8, 2, strech);
    this.LeftWing[1].setRotationPoint(0.0F, f, 0.0F);
    this.LeftWing[2] = new ModelRenderer(this, 56, 16);
    this.LeftWing[2].mirror = true;
    this.LeftWing[2].addBox(4.0F, 5.0F, 6.0F, 2, 6, 2, strech);
    this.LeftWing[2].setRotationPoint(0.0F, f, 0.0F);


    this.RightWing = new ModelRenderer[3];

    this.RightWing[0] = new ModelRenderer(this, 56, 16);
    this.RightWing[0].addBox(-6.0F, 5.0F, 2.0F, 2, 6, 2, strech);
    this.RightWing[0].setRotationPoint(0.0F, f, 0.0F);
    this.RightWing[1] = new ModelRenderer(this, 56, 16);
    this.RightWing[1].addBox(-6.0F, 5.0F, 4.0F, 2, 8, 2, strech);
    this.RightWing[1].setRotationPoint(0.0F, f, 0.0F);
    this.RightWing[2] = new ModelRenderer(this, 56, 16);
    this.RightWing[2].addBox(-6.0F, 5.0F, 6.0F, 2, 6, 2, strech);
    this.RightWing[2].setRotationPoint(0.0F, f, 0.0F);


    this.LeftWingExt = new ModelRenderer[7];
    
    this.LeftWingExt[0] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[0].mirror = true;
    this.LeftWingExt[0].addBox(0.0F, 0.0F, 0.0F, 1, 8, 2, strech + 0.1F);
    this.LeftWingExt[0].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[1] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[1].mirror = true;
    this.LeftWingExt[1].addBox(0.0F, 8.0F, 0.0F, 1, 6, 2, strech + 0.1F);
    this.LeftWingExt[1].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[2] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[2].mirror = true;
    this.LeftWingExt[2].addBox(0.0F, -1.2F, -0.2F, 1, 8, 2, strech - 0.2F);
    this.LeftWingExt[2].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[3] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[3].mirror = true;
    this.LeftWingExt[3].addBox(0.0F, 1.8F, 1.3F, 1, 8, 2, strech - 0.1F);
    this.LeftWingExt[3].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[4] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[4].mirror = true;
    this.LeftWingExt[4].addBox(0.0F, 5.0F, 2.0F, 1, 8, 2, strech);
    this.LeftWingExt[4].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[5] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[5].mirror = true;
    this.LeftWingExt[5].addBox(0.0F, 0.0F, -0.2F, 1, 6, 2, strech + 0.3F);
    this.LeftWingExt[5].setRotationPoint(4.5F, 5.0F + f, 6.0F);
    this.LeftWingExt[6] = new ModelRenderer(this, 56, 19);
    this.LeftWingExt[6].mirror = true;
    this.LeftWingExt[6].addBox(0.0F, 0.0F, 0.2F, 1, 3, 2, strech + 0.2F);
    this.LeftWingExt[6].setRotationPoint(4.5F, 5.0F + f, 6.0F);


    this.RightWingExt = new ModelRenderer[7];
    
    this.RightWingExt[0] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[0].mirror = true;
    this.RightWingExt[0].addBox(0.0F, 0.0F, 0.0F, 1, 8, 2, strech + 0.1F);
    this.RightWingExt[0].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[1] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[1].mirror = true;
    this.RightWingExt[1].addBox(0.0F, 8.0F, 0.0F, 1, 6, 2, strech + 0.1F);
    this.RightWingExt[1].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[2] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[2].mirror = true;
    this.RightWingExt[2].addBox(0.0F, -1.2F, -0.2F, 1, 8, 2, strech - 0.2F);
    this.RightWingExt[2].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[3] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[3].mirror = true;
    this.RightWingExt[3].addBox(0.0F, 1.8F, 1.3F, 1, 8, 2, strech - 0.1F);
    this.RightWingExt[3].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[4] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[4].mirror = true;
    this.RightWingExt[4].addBox(0.0F, 5.0F, 2.0F, 1, 8, 2, strech);
    this.RightWingExt[4].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[5] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[5].mirror = true;
    this.RightWingExt[5].addBox(0.0F, 0.0F, -0.2F, 1, 6, 2, strech + 0.3F);
    this.RightWingExt[5].setRotationPoint(-4.5F, 5.0F + f, 6.0F);
    this.RightWingExt[6] = new ModelRenderer(this, 56, 19);
    this.RightWingExt[6].mirror = true;
    this.RightWingExt[6].addBox(0.0F, 0.0F, 0.2F, 1, 3, 2, strech + 0.2F);
    this.RightWingExt[6].setRotationPoint(-4.5F, 5.0F + f, 6.0F);


    this.WingRotateAngleX = this.LeftWingExt[0].rotateAngleX;
    this.WingRotateAngleY = this.LeftWingExt[0].rotateAngleY;
    this.WingRotateAngleZ = this.LeftWingExt[0].rotateAngleZ;
  }

  
  public void setRHead(float Y, float X) {
	float rotateAngleY, rotateAngleX;
	
    if (this.isSleeping) {
    	rotateAngleY = 1.4F;
    	rotateAngleX = 0.1F;
	} else {
		rotateAngleY = Y / 57.29578F;
		rotateAngleX = X / 57.29578F;
	}
    
	this.Head.rotateAngleY = rotateAngleY;
	this.Head.rotateAngleX = rotateAngleX;
	
	setRHeadpiece(rotateAngleY, rotateAngleX);
	setRHelmet(rotateAngleY, rotateAngleX);
	setRBodypieceNeck();
  }
  
  public void setRHeadpiece(float rotateAngleY, float rotateAngleX) {
	this.Headpiece[0].rotateAngleY = rotateAngleY;
	this.Headpiece[0].rotateAngleX = rotateAngleX;
	this.Headpiece[1].rotateAngleY = rotateAngleY;
	this.Headpiece[1].rotateAngleX = rotateAngleX;
	this.Headpiece[2].rotateAngleY = rotateAngleY;
	this.Headpiece[2].rotateAngleX = rotateAngleX + 0.5F;
  }
  
  public void setRHelmet(float rotateAngleY, float rotateAngleX) {
	  this.Helmet.rotateAngleY = rotateAngleY;
	  this.Helmet.rotateAngleX = rotateAngleX;
  }
  
  public void setRBodypieceNeck() {
	  this.BodypieceNeck[0].rotateAngleX = 0.166F;
	  this.BodypieceNeck[1].rotateAngleX = 0.166F;
	  this.BodypieceNeck[2].rotateAngleX = 0.166F;
	  this.BodypieceNeck[3].rotateAngleX = 0.166F;
  }
  
  public void setRLegs(float f, float f1) {
	setMainRLegs(f, f1);
	  
	if (this.heldItemRight != 0 && !this.rainboom && !this.isUnicorn) {
		this.RightArm.rotateAngleX = this.RightArm.rotateAngleX * 0.5F - 0.3141593F;
	}
	  
	float rotationPointZ = MathHelper.sin(this.Body.rotateAngleY) * 5.0F;
	  
	    
	if (this.rainboom) {
		this.RightArm.rotationPointZ = rotationPointZ + 2.0F;
		this.LeftArm.rotationPointZ = 0.0F - rotationPointZ + 2.0F;
	} else {
		this.RightArm.rotationPointZ = rotationPointZ + 1.0F;
		this.LeftArm.rotationPointZ = 0.0F - rotationPointZ + 1.0F;
	}
	
	setRPointLegs();
  }
  
  public void setRPointLegs() {
	  float f14 = MathHelper.cos(this.Body.rotateAngleY) * 5.0F;
	  float f15 = 4.0F;
	    
	  if (this.isSneak && !this.isFlying)
		  f15 = 0.0F;
	    
	  if (this.isSleeping)
		  f15 = 2.6F;
	  
	  this.RightArm.rotationPointX = 0.0F - f14 - 1.0F + f15;
	  this.LeftArm.rotationPointX = f14 + 1.0F - f15;
	  this.RightLeg.rotationPointX = 0.0F - f14 - 1.0F + f15;
	  this.LeftLeg.rotationPointX =f14 + 1.0F - f15;
	  this.RightArm.rotateAngleY += this.Body.rotateAngleY;
	  this.LeftArm.rotateAngleY += this.Body.rotateAngleY;
	  this.LeftArm.rotateAngleX += this.Body.rotateAngleY;
	  this.RightArm.rotationPointY = 8.0F;
	  this.LeftArm.rotationPointY = this.RightArm.rotationPointY;
	  this.RightLeg.rotationPointY = 4.0F;
	  this.LeftLeg.rotationPointY = this.LeftLeg.rotationPointY;
  }
  
  public void setMainRLegs(float f, float f1) {
	  float RightArmRotateAngleX, LeftArmRotateAngleX;
	  float RightLegRotateAngleX, LeftLegRotateAngleX;
	  
	  
	  if (!this.isFlying || !this.isPegasus) {
		  RightArmRotateAngleX = MathHelper.sin(f * 0.6662F + 3.141593F) * 0.6F * f1;
		  LeftArmRotateAngleX = MathHelper.sin(f * 0.6662F) * 0.6F * f1;
		  RightLegRotateAngleX = MathHelper.sin(f * 0.6662F) * 0.3F * f1;
		  LeftLegRotateAngleX = MathHelper.sin(f * 0.6662F + 3.141593F) * 0.3F * f1;
			  
		  this.RightArm.rotateAngleY = 0.0F;
		  this.unicornarm.rotateAngleY = 0.0F;
		  this.LeftArm.rotateAngleY = 0.0F;
		  this.RightLeg.rotateAngleY = 0.0F;
		  this.LeftLeg.rotateAngleY = 0.0F;
		      
	  } else {
		  if (f1 < 0.9999F) {
			  this.rainboom = false;
			        
			  RightArmRotateAngleX = MathHelper.cos(0.0F - f1 * 0.5F);
			  LeftArmRotateAngleX = MathHelper.cos(0.0F - f1 * 0.5F);
			  RightLegRotateAngleX = MathHelper.cos(f1 * 0.5F);
			  LeftLegRotateAngleX = MathHelper.cos(f1 * 0.5F);
		  } else {
			  this.rainboom=true;
		        
			  RightArmRotateAngleX = 4.712F;
			  LeftArmRotateAngleX = RightArmRotateAngleX;
			  RightLegRotateAngleX = 1.571F;
			  LeftLegRotateAngleX = RightLegRotateAngleX;
		  }
		      
		  this.RightArm.rotateAngleY = 0.2F;
		  this.LeftArm.rotateAngleY = -0.2F;
		  this.RightLeg.rotateAngleY = this.LeftArm.rotateAngleY;
		  this.LeftLeg.rotateAngleY = this.RightArm.rotateAngleY;
	  }
	  
	  if (this.isSleeping) {
		  RightArmRotateAngleX = 4.712F;
		  LeftArmRotateAngleX = RightArmRotateAngleX;
		  RightLegRotateAngleX = 1.571F;
		  LeftLegRotateAngleX = RightLegRotateAngleX;
	  }
		
		this.RightArm.rotateAngleX = RightArmRotateAngleX;
		this.LeftArm.rotateAngleX = LeftArmRotateAngleX;
		this.RightLeg.rotateAngleX = RightLegRotateAngleX;
		this.LeftLeg.rotateAngleX = LeftLegRotateAngleX;
	  
		this.unicornarm.rotateAngleX = 0.0F;
	  
		this.RightArm.rotateAngleZ = 0.0F;
		this.unicornarm.rotateAngleZ = 0.0F;
		this.LeftArm.rotateAngleZ = 0.0F;
  }
  
  public void setRTail(float f, float f1) {
	  for (int i=0; i < this.Tail.length; i++) {
		  if (this.rainboom) {
			  (this.Tail[i]).rotateAngleZ = 0.0F;
	      } else {
	    	  (this.Tail[i]).rotateAngleZ = MathHelper.cos(f * 0.8F) * 0.2F * f1;
	      }
	  }
  }
  
  public void setRFBody(float f5) {
	float rotateAngleY = 0.0F;
	    
	if (f5 > -9990.0F && !this.isUnicorn)
		rotateAngleY = MathHelper.sin(MathHelper.sin(f5) * 3.141593F * 2.0F) * 0.2F;
	    
		this.Body.rotateAngleY = (float)(rotateAngleY * 0.2D);
	    
	    for (int i = 0, blength = this.Bodypiece.length; i < blength; i++) {
	    	(this.Bodypiece[i]).rotateAngleY = (float)(rotateAngleY * 0.2D);
	    }
	    
	    for (int i = 0, blength = this.LeftWing.length; i < blength; i++) {
	    	(this.LeftWing[i]).rotateAngleY = (float)(rotateAngleY * 0.2D);
	    }
	    
	    for (int i = 0, blength = this.RightWing.length; i < blength; i++) {
	    	(this.RightWing[i]).rotateAngleY = (float)(rotateAngleY * 0.2D);
	    }
	    
	    for (int i = 0, blength = this.Tail.length; i < blength; i++) {
	    	(this.Tail[i]).rotateAngleY = rotateAngleY;
	    }
  	}
  
  public void setRUnicorn(float f5) {
	if (f5 > -9990.0F) {
		float f = f5;
		f = 1.0F - f5;
		f *= f * f;
		f = 1.0F - f;
		
		float rotateAngleX = MathHelper.sin(f * 3.141593F);
		float rotateAngleZ = MathHelper.sin(f5 * 3.141593F);
		float f33 = rotateAngleZ * -(this.Head.rotateAngleX - 0.7F) * 0.75F;
		
		if (this.isUnicorn) {
			this.unicornarm.rotateAngleX = (float)(this.unicornarm.rotateAngleX - rotateAngleX * 1.2D + f33);
			this.unicornarm.rotateAngleY += this.Body.rotateAngleY * 2.0F;
			this.unicornarm.rotateAngleZ = rotateAngleZ * -0.4F;
		} else {
			this.unicornarm.rotateAngleX = (float)(this.unicornarm.rotateAngleX - rotateAngleX * 1.2D + f33);
			this.unicornarm.rotateAngleY += this.Body.rotateAngleY * 2.0F;
			this.unicornarm.rotateAngleZ = rotateAngleZ * -0.4F;
		}
	}
  }
  

  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
    EntityNPCInterface npc = (EntityNPCInterface)entity;
    this.isRiding = npc.isRiding();
    
    if (this.isSneak && (npc.currentAnimation == EnumAnimation.CRAWLING || npc.currentAnimation == EnumAnimation.LYING))
      this.isSneak = false;
    this.rainboom = false;
    
    setRHead(f3, f4);
    setRTail(f, f1);
    setRFBody(f5);
    setRLegs(f, f1);
    setRUnicorn(f5);

    if (this.isSneak && !this.isFlying) {
      this.Body.rotateAngleX = 0.4F;
      this.Body.rotationPointY = 7.0F;
      this.Body.rotationPointZ = -4.0F;
      
      for (int i = 0, blength = this.BodypieceNeck.length; i < blength; i++) {
          (this.BodypieceNeck[i]).rotateAngleX = 0.166F + 0.4F;
          (this.BodypieceNeck[i]).rotationPointY = 7.0F;
          (this.BodypieceNeck[i]).rotationPointZ = -4.0F;
        }
      
      for (int i = 0, blength = this.Bodypiece.length; i < blength; i++) {
        (this.Bodypiece[i]).rotateAngleX = 0.4F;
        (this.Bodypiece[i]).rotationPointY = 7.0F;
        (this.Bodypiece[i]).rotationPointZ = -4.0F;
      }
      

      for (int i = 0, blength = this.LeftWingExt.length; i < blength; i++) {
        (this.LeftWingExt[i]).rotateAngleX = (float)(0.4F + 2.3561947345733643D);
        (this.LeftWingExt[i]).rotationPointY = 7.0F + 3.5F;
        (this.LeftWingExt[i]).rotationPointZ = -4.0F + 6.0F;
        (this.LeftWingExt[i]).rotateAngleX = 2.5F;
        (this.LeftWingExt[i]).rotateAngleZ = -6.0F;
      }
      
      for (int i = 0, blength = this.RightWingExt.length; i < blength; i++) {
        (this.RightWingExt[i]).rotateAngleX = (float)(0.4F  + 2.3561947345733643D);
        (this.RightWingExt[i]).rotationPointY = 7.0F + 4.5F;
        (this.RightWingExt[i]).rotationPointZ = -4.0F + 6.0F;
        (this.RightWingExt[i]).rotateAngleX = 2.5F;
        (this.RightWingExt[i]).rotateAngleZ = 6.0F;
      }
      
      this.RightLeg.rotateAngleX -= 0.0F;
      this.LeftLeg.rotateAngleX -= 0.0F;
      this.RightArm.rotateAngleX -= 0.4F;
      this.unicornarm.rotateAngleX += 0.4F;
      this.LeftArm.rotateAngleX -= 0.4F;
      this.RightLeg.rotationPointZ = 10.0F;
      this.LeftLeg.rotationPointZ = 10.0F;
      this.RightLeg.rotationPointY = 7.0F;
      this.LeftLeg.rotationPointY = 7.0F;
      
      
      float fSleepY = 0.0F;
      float fSleepZ = 0.0F;
      float fSleepX = 0.0F;
      
      if (this.isSleeping) {
    	  fSleepY = 2.0F;
    	  fSleepZ = -1.0F;
    	  fSleepX = 1.0F;
      } else {
    	  fSleepY = 6.0F;
    	  fSleepZ = -2.0F;
    	  fSleepX = 0.0F;
      }
      
      this.Head.rotationPointY = fSleepY;
      this.Head.rotationPointZ = fSleepZ;
      this.Head.rotationPointX = fSleepX;
      
      this.Helmet.rotationPointY = fSleepY;
      this.Helmet.rotationPointZ = fSleepZ;
      this.Helmet.rotationPointX = fSleepX;
      
      for(int i = 0; i < this.Headpiece.length; i++) {
  		(this.Headpiece[i]).rotationPointY = fSleepY;
          (this.Headpiece[i]).rotationPointZ = fSleepZ;
          (this.Headpiece[i]).rotationPointX = fSleepX;
      }
      
      for (int i = 0; i < this.Tail.length; i++) {
        (this.Tail[i]).rotationPointX = 0.0F;
        (this.Tail[i]).rotationPointY = 1.0F;
        (this.Tail[i]).rotationPointZ = 10.0F;
        (this.Tail[i]).rotateAngleX = 0.0F;
      }
      
    } else {
      this.Body.rotateAngleX = 0.0F;
      this.Body.rotationPointY = 0.0F;
      this.Body.rotationPointZ = 0.0F;
      
      for (int i = 0, blength = this.BodypieceNeck.length; i < blength; i++) {
          (this.BodypieceNeck[i]).rotateAngleX = 0.166F;
          (this.BodypieceNeck[i]).rotationPointY = 0.0F;
          (this.BodypieceNeck[i]).rotationPointZ = 0.0F;
        }
      
      for (int i = 0; i < this.Bodypiece.length; i++) {
        (this.Bodypiece[i]).rotateAngleX = 0.0F;
        (this.Bodypiece[i]).rotationPointY = 0.0F;
        (this.Bodypiece[i]).rotationPointZ = 0.0F;
      }
      
      if (this.isPegasus) {
        if (!this.isFlying) {
          for (int i = 0; i < this.LeftWing.length; i++) {
            (this.LeftWing[i]).rotateAngleX = (float)(1.5707964897155762D);
            (this.LeftWing[i]).rotationPointY = 13.0F;
            (this.LeftWing[i]).rotationPointZ = -3.0F;
          }
          for (int i = 0; i < this.RightWing.length; i++) {
            (this.RightWing[i]).rotateAngleX = (float)(1.5707964897155762D);
            (this.RightWing[i]).rotationPointY = 13.0F;
            (this.RightWing[i]).rotationPointZ = -3.0F;
          }
        } else {
          for (int i = 0; i < this.LeftWingExt.length; i++) {
            (this.LeftWingExt[i]).rotateAngleX=(float)(1.5707964897155762D);
            (this.LeftWingExt[i]).rotationPointY = 5.5F;
            (this.LeftWingExt[i]).rotationPointZ = 3.0F;
          }

          for (int i = 0; i < this.RightWingExt.length; i++) {
            (this.RightWingExt[i]).rotateAngleX = (float)(1.5707964897155762D);
            (this.RightWingExt[i]).rotationPointY = 6.5F;
            (this.RightWingExt[i]).rotationPointZ = 3.0F;
          }
        }
      }
      
      this.RightLeg.rotationPointZ = 10.0F;
      this.LeftLeg.rotationPointZ = 10.0F;
      this.RightLeg.rotationPointY = 8.0F;
      this.LeftLeg.rotationPointY = 8.0F;
      
      this.unicornarm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
      this.unicornarm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
      
      if (this.isPegasus && this.isFlying) {
    	  
        this.WingRotateAngleY=MathHelper.sin(f2 * 0.067F * 8.0F) * 1.0F;
        this.WingRotateAngleZ=MathHelper.sin(f2 * 0.067F * 8.0F) * 1.0F;
        
        for (int i = 0; i < this.LeftWingExt.length; i++) {
          (this.LeftWingExt[i]).rotateAngleX = 2.5F;
          (this.LeftWingExt[i]).rotateAngleZ = -this.WingRotateAngleZ - 4.712F - 0.4F;
        }
        
        for (int i = 0; i < this.RightWingExt.length; i++) {
          (this.RightWingExt[i]).rotateAngleX = 2.5F;
          (this.RightWingExt[i]).rotateAngleZ = this.WingRotateAngleZ + 4.712F + 0.4F;
        }
      }
      
      float fSleepY = 0.0F;
      float fSleepZ = 0.0F;
      float fSleepX = 0.0F;
      
      if (this.isSleeping) {
    	  fSleepY = 2.0F;
    	  fSleepZ = 1.0F;
    	  fSleepX = 1.0F;
      }
      
      this.Head.rotationPointY = fSleepY;
      this.Head.rotationPointZ = fSleepZ;
      this.Head.rotationPointX = fSleepX;
      
      for (int i = 0; i < this.Tail.length; i++) {
        (this.Tail[i]).rotationPointX = 0.0F;
        (this.Tail[i]).rotationPointY = 1.0F;
        (this.Tail[i]).rotationPointZ = 14.0F;
        if (this.rainboom) {
          (this.Tail[i]).rotateAngleX = 1.571F + 0.1F * MathHelper.sin(f);
        } else {
          (this.Tail[i]).rotateAngleX = 0.5F * f1;
        }
      }
      
      for (int l5=0; l5 < this.Tail.length; l5++) {
        if (!this.rainboom)
          (this.Tail[l5]).rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
      }
    }
    
    this.LeftWingExt[2].rotateAngleX -= 0.85F;
    this.LeftWingExt[3].rotateAngleX -= 0.75F;
    this.LeftWingExt[4].rotateAngleX -= 0.5F;
    this.LeftWingExt[6].rotateAngleX -= 0.85F;
    this.RightWingExt[2].rotateAngleX -= 0.85F;
    this.RightWingExt[3].rotateAngleX -= 0.75F;
    this.RightWingExt[4].rotateAngleX -= 0.5F;
    this.RightWingExt[6].rotateAngleX -= 0.85F;
    this.Bodypiece[9].rotateAngleX += 0.5F;
    this.Bodypiece[10].rotateAngleX += 0.5F;
    this.Bodypiece[11].rotateAngleX += 0.5F;
    this.Bodypiece[12].rotateAngleX += 0.5F;
    this.Bodypiece[13].rotateAngleX += 0.5F;
    
    if (this.rainboom) {
      for (int j1=0; j1 < this.Tail.length; j1++) {
        (this.Tail[j1]).rotationPointY += 6.0F;
        (this.Tail[j1]).rotationPointZ++;
      }
    }
    
    if (this.isSleeping) {
      this.RightArm.rotationPointZ += 6.0F;
      this.LeftArm.rotationPointZ += 6.0F;
      this.RightLeg.rotationPointZ -= 8.0F;
      this.LeftLeg.rotationPointZ -= 8.0F;
      this.RightArm.rotationPointY += 2.0F;
      this.LeftArm.rotationPointY += 2.0F;
      this.RightLeg.rotationPointY += 2.0F;
      this.LeftLeg.rotationPointY += 2.0F;
    }

    if(this.isRiding) {
        this.RightArm.rotateAngleX += -0.261799F;
        this.LeftArm.rotateAngleX += this.RightArm.rotateAngleX;
        
        this.RightArm.rotationPointY = 0.0F;
        this.RightArm.rotationPointZ = -2.5F;
        
        this.LeftArm.rotationPointY = this.RightArm.rotationPointY;
        this.LeftArm.rotationPointZ = this.RightArm.rotationPointZ;
        
        this.RightLeg.rotateAngleX = -1.5708F;
        this.LeftLeg.rotateAngleX = this.RightLeg.rotateAngleX;
        
        this.RightLeg.rotateAngleY = 0.31415927F;
        this.LeftLeg.rotateAngleY = -0.31415927F;
        
        this.RightLeg.rotationPointY = 14.0F;
        this.LeftLeg.rotationPointY = this.RightLeg.rotationPointY;
        
        this.Body.rotateAngleX += -0.959931F;
        this.Body.rotationPointY = -2.5F;
        this.Body.rotationPointZ = 5.0F;
        
        for(int i = 0; i < this.Bodypiece.length; i++) {
        	this.Bodypiece[i].rotateAngleX += this.Body.rotateAngleX;
        	this.Bodypiece[i].rotationPointY = this.Body.rotationPointY;
        	this.Bodypiece[i].rotationPointZ = this.Body.rotationPointZ;
        }
        
        for(int i = 0; i < this.BodypieceNeck.length; i++) {
        	this.BodypieceNeck[i].rotationPointY = -5.5F;
        	this.BodypieceNeck[i].rotationPointZ = -0.5F;
        }
        
        this.Head.rotationPointY = -5.5F;
        this.Head.rotationPointZ = -0.5F;
        
        for (int i = 0; i < this.Tail.length; i++) {
            (this.Tail[i]).rotationPointY = 10.0F;
            (this.Tail[i]).rotationPointZ = 13.5F;
            (this.Tail[i]).rotationPointX = -1.0F;
            
            (this.Tail[i]).rotateAngleX = -1.48353F;
            (this.Tail[i]).rotateAngleY = -0.872665F;
        }
        
     }
    
    setRAimedBow(f2, npc);
    
    this.Helmet.rotationPointY = this.Head.rotationPointY;
    this.Helmet.rotationPointZ = this.Head.rotationPointZ;
    this.Helmet.rotationPointX = this.Head.rotationPointX;
    
    for(int i = 0; i < this.Headpiece.length; i++) {
  		(this.Headpiece[i]).rotationPointY = this.Head.rotationPointY;
        (this.Headpiece[i]).rotationPointZ = this.Head.rotationPointZ;
        (this.Headpiece[i]).rotationPointX = this.Head.rotationPointX;
      }
    
	if(this.isMale) {
      	for(int i = 0; i < this.MuzzleMale.length; i++) {
      		(this.MuzzleMale[i]).rotationPointY = this.Head.rotationPointY;
      		(this.MuzzleMale[i]).rotationPointZ = this.Head.rotationPointZ;
      		(this.MuzzleMale[i]).rotationPointX = this.Head.rotationPointX;
      		
      		(this.MuzzleMale[i]).rotateAngleY = this.Head.rotateAngleY;
      		(this.MuzzleMale[i]).rotateAngleZ = this.Head.rotateAngleZ;
      		(this.MuzzleMale[i]).rotateAngleX = this.Head.rotateAngleX;
      	}
      } else {
      	for(int i = 0; i < this.MuzzleFemale.length; i++) {
      		(this.MuzzleFemale[i]).rotationPointY = this.Head.rotationPointY;
      		(this.MuzzleFemale[i]).rotationPointZ = this.Head.rotationPointZ;
      		(this.MuzzleFemale[i]).rotationPointX = this.Head.rotationPointX;
      		
      		(this.MuzzleFemale[i]).rotateAngleY = this.Head.rotateAngleY;
      		(this.MuzzleFemale[i]).rotateAngleZ = this.Head.rotateAngleZ;
      		(this.MuzzleFemale[i]).rotateAngleX = this.Head.rotateAngleX;
      	}
      }
  }
  
  public void setRAimedBow(float f2, EntityNPCInterface npc) {
	if (this.aimedBow || npc.currentAnimation == EnumAnimation.AIMING) {
		if (this.isUnicorn) {
	        this.unicornarm.rotateAngleZ = 0.0F;
	        this.unicornarm.rotateAngleX = -1.5707964F + this.Head.rotateAngleX;
	        this.unicornarm.rotateAngleX -= 0.0F;
	        
	        this.unicornarm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
	        this.unicornarm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
	        
	        this.unicornarm.rotateAngleY = -0.1F + this.Head.rotateAngleY;
		} else {
			float rotationPointZ = MathHelper.sin(this.Body.rotateAngleY) * 5.0F;
			
			this.RightArm.rotationPointZ = rotationPointZ + 2.0F;

			
			this.RightArm.rotateAngleZ = 0.0F;
	        this.RightArm.rotateAngleX = -1.5707964F + this.Head.rotateAngleX;
	        this.RightArm.rotateAngleX -= 0.0F;
	        
	        this.RightArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
	        this.RightArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
	        
	        this.RightArm.rotateAngleY = -0.1F + this.Head.rotateAngleY;
		}
	}
  }

  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
    EntityNpcPony pony = (EntityNpcPony)entity;
    if (pony.textureLocation != pony.checked && pony.textureLocation != null)
      try {
        IResource resource=Minecraft.getMinecraft().getResourceManager().getResource(pony.textureLocation);
        BufferedImage bufferedimage=ImageIO.read(resource.getInputStream());
        pony.isPegasus = false;
        pony.isUnicorn = false;
        pony.isMale = false;
        
        Color color = new Color(bufferedimage.getRGB(0, 0), true);
        Color color1 = new Color(249, 177, 49, 255);
        Color color2 = new Color(136, 202, 240, 255);
        Color color3 = new Color(209, 159, 228, 255);
        Color color4 = new Color(254, 249, 252, 255);
        if (color.equals(color1));
        if (color.equals(color2))
          pony.isPegasus = true;
        if (color.equals(color3))
          pony.isUnicorn = true;
        if (color.equals(color4)) {
          pony.isPegasus = true;
          pony.isUnicorn = true;
        }
        
        Color male = new Color(bufferedimage.getRGB(3, 0), true);
        Color maleColor = new Color(255, 255, 255, 1);

        
        if(male.equals(maleColor)) {
        	pony.isMale = true;
        }
        
        pony.checked=pony.textureLocation;
      } catch (IOException e) {}
    this.isSleeping = pony.isPlayerSleeping();
    this.isUnicorn = pony.isUnicorn;
    this.isPegasus = pony.isPegasus;
    this.isMale = pony.isMale;
    this.isSneak = pony.isSneaking();
    this.heldItemRight = (pony.getHeldItem() == null) ? 0 : 1;
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    GL11.glPushMatrix();
    if (this.isSleeping) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.5F, -0.9F);
    }
    float scale=f5;
    this.Head.render(scale);
    this.Headpiece[0].render(scale);
    this.Headpiece[1].render(scale);
    if (this.isUnicorn)
      this.Headpiece[2].render(scale);
    this.Helmet.render(scale);
    this.Body.render(scale);
    
    for (int i152=0; i152 < this.BodypieceNeck.length; i152++)
        (this.BodypieceNeck[i152]).render(scale);
    
    for (int i=0; i < this.Bodypiece.length; i++)
    	(this.Bodypiece[i]).render(scale);
    
    this.LeftArm.render(scale);
    this.RightArm.render(scale);
    this.LeftLeg.render(scale);
    this.RightLeg.render(scale);
    
    for (int j=0; j < this.Tail.length; j++)
    	(this.Tail[j]).render(scale);
    if (this.isPegasus)
      if (this.isFlying || this.isSneak) {
        for (int k=0; k < this.LeftWingExt.length; k++)
        	(this.LeftWingExt[k]).render(scale);
        for (int l=0; l < this.RightWingExt.length; l++)
        	(this.RightWingExt[l]).render(scale);
      } else {
        for (int i1=0; i1 < this.LeftWing.length; i1++)
        	(this.LeftWing[i1]).render(scale);
        for (int j1=0; j1 < this.RightWing.length; j1++)
        	(this.RightWing[j1]).render(scale);
      }
    
    if (this.isMale) {
    	for (int i = 0; i < this.MuzzleMale.length; i++) {
    		(this.MuzzleMale[i]).render(scale);
    	}
    } else {
    	for (int i = 0; i < this.MuzzleFemale.length; i++) {
    		(this.MuzzleFemale[i]).render(scale);
    	}
    }
    
    GL11.glPopMatrix();
  }

  protected void renderGlow(RenderManager rendermanager, EntityPlayer entityplayer) {
    ItemStack itemstack = entityplayer.inventory.getCurrentItem();
    if (itemstack == null)
      return;
    GL11.glPushMatrix();
    double d=entityplayer.posX;
    double d1=entityplayer.posY;
    double d2=entityplayer.posZ;
    GL11.glEnable(32826);
    GL11.glTranslatef((float)d + 0.0F, (float)d1 + 2.3F, (float)d2);
    GL11.glScalef(5.0F, 5.0F, 5.0F);
    GL11.glRotatef(-rendermanager.playerViewY, 0.0F, 1.0F, 0.0F);
    GL11.glRotatef(rendermanager.playerViewX, 1.0F, 0.0F, 0.0F);
    Tessellator tessellator = Tessellator.instance;

    tessellator.startDrawingQuads();
    tessellator.setNormal(0.0F, 1.0F, 0.0F);
    tessellator.addVertexWithUV(-1.0D, -1.0D, 0.0D, 0.0D, 1.0D);
    tessellator.addVertexWithUV(-1.0D, 1.0D, 0.0D, 1.0D, 1.0D);
    tessellator.addVertexWithUV(1.0D, 1.0D, 0.0D, 1.0D, 0.0D);
    tessellator.addVertexWithUV(1.0D, -1.0D, 0.0D, 0.0D, 0.0D);
    tessellator.draw();
    GL11.glDisable(32826);
    GL11.glPopMatrix();
  }
}
