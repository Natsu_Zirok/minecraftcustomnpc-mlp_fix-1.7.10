package noppes.npcs.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import noppes.npcs.client.model.util.ModelPlaneRenderer;


public class ModelPonyArmor extends ModelBase {
  
  public ModelRenderer Head;
  
  public ModelRenderer Body;
  
  public ModelRenderer BodyBack;
  
  public ModelRenderer RightArm;
  
  public ModelRenderer LeftArm;
  
  public ModelRenderer RightLeg;
  
  public ModelRenderer LeftLeg;
  
  public ModelRenderer RightArm2;
  
  public ModelRenderer LeftArm2;
  
  public ModelRenderer RightLeg2;
  
  public ModelRenderer LeftLeg2;
  
  public boolean isPegasus = false;
  
  public boolean isUnicorn = false;
  
  public boolean isSleeping = false;
  
  public boolean isFlying = false;
  
  public boolean isGlow = false;
  
  public boolean isSneak = false;
  
  public boolean aimedBow;
  
  public int heldItemRight;
  
  public ModelPony Owner = null;
  
  public ModelPonyArmor(float f, ModelPony modelBipedMain) {
	  this.Owner = modelBipedMain;
    init(f, 0.0F);
  }
  
  public void init(float strech, float f) {
    this.Head = new ModelRenderer(this, 0, 0);
    this.Head.addBox(-4.0F, -5.0F, -6.0F, 8, 8, 8, strech);
    this.Head.setRotationPoint(0.0F, 0.0F, 0.0F);
    
    this.Body = new ModelRenderer(this, 16, 16);
    this.Body.addBox(-4.0F, 4.0F, -2.0F, 8, 8, 4, strech);
    this.Body.setRotationPoint(0.0F, f, 0.0F);
    
    this.BodyBack = new ModelRenderer(this, 0, 0);
    this.BodyBack.addBox(-4.0F, 4.0F, 6.0F, 8, 8, 8, strech);
    this.BodyBack.setRotationPoint(0.0F, f, 0.0F);
    
    this.RightArm = new ModelRenderer(this, 0, 16);
    this.RightArm.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.RightArm.setRotationPoint(-3.0F, 8.0F + f, 0.0F);
    
    this.LeftArm = new ModelRenderer(this, 0, 16);
    this.LeftArm.mirror = true;
    this.LeftArm.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.LeftArm.setRotationPoint(3.0F, 8.0F + f, 0.0F);
    
    this.RightLeg = new ModelRenderer(this, 0, 16);
    this.RightLeg.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.RightLeg.setRotationPoint(-3.0F, f, 0.0F);
    
    this.LeftLeg = new ModelRenderer(this, 0, 16);
    this.LeftLeg.mirror = true;
    this.LeftLeg.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech);
    this.LeftLeg.setRotationPoint(3.0F, f, 0.0F);
    
    this.RightArm2 = new ModelRenderer(this, 0, 16);
    this.RightArm2.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech * 0.5F);
    this.RightArm2.setRotationPoint(-3.0F, 8.0F + f, 0.0F);
    
    this.LeftArm2 = new ModelRenderer(this, 0, 16);
    this.LeftArm2.mirror = true;
    this.LeftArm2.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech * 0.5F);
    this.LeftArm2.setRotationPoint(3.0F, 8.0F + f, 0.0F);
    
    this.RightLeg2 = new ModelRenderer(this, 0, 16);
    this.RightLeg2.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech * 0.5F);
    this.RightLeg2.setRotationPoint(-3.0F, f, 0.0F);
    
    this.LeftLeg2 = new ModelRenderer(this, 0, 16);
    this.LeftLeg2.mirror = true;
    this.LeftLeg2.addBox(-2.0F, 4.0F, -2.0F, 4, 12, 4, strech * 0.5F);
    this.LeftLeg2.setRotationPoint(3.0F, f, 0.0F);
  }
/*
  
  public void setRHead(float Y, float X) {
	float rotateAngleY, rotateAngleX;
	
    if (this.isSleeping) {
    	rotateAngleY = 1.4F;
    	rotateAngleX = 0.1F;
	} else {
		rotateAngleY = Y / 57.29578F;
		rotateAngleX = X / 57.29578F;
	}
    
	this.Head.rotateAngleY = rotateAngleY;
	this.Head.rotateAngleX = rotateAngleX;
  }

  
  public void setRLegs(float f, float f1) {
	setMainRLegs(f, f1);
	  
	if (this.heldItemRight != 0 && !this.rainboom && !this.isUnicorn) {
		this.RightArm.rotateAngleX = this.RightArm.rotateAngleX * 0.5F - 0.3141593F;
		this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
	}
	  
	float rotationPointZ = MathHelper.sin(this.Body.rotateAngleY) * 5.0F;
	    
	if (this.rainboom) {
		this.RightArm.rotationPointZ = rotationPointZ + 2.0F;
		this.RightArm2.rotationPointZ = this.RightArm.rotationPointZ;
		
		this.LeftArm.rotationPointZ = 0.0F - rotationPointZ + 2.0F;
		this.LeftArm2.rotationPointZ = this.LeftArm.rotationPointZ;
	} else {
		this.RightArm.rotationPointZ = rotationPointZ + 1.0F;
		this.RightArm2.rotationPointZ = this.RightArm.rotationPointZ;
		
		this.LeftArm.rotationPointZ = 0.0F - rotationPointZ + 1.0F;
		this.LeftArm2.rotationPointZ = this.LeftArm.rotationPointZ;
	}
	
	setRPointLegs();
  }
  
  public void setRPointLegs() {
	  float f14 = MathHelper.cos(this.Body.rotateAngleY) * 5.0F;
	  float f15 = 4.0F;
	    
	  if (this.isSneak && !this.isFlying)
		  f15 = 0.0F;
	    
	  if (this.isSleeping)
		  f15 = 2.6F;
	  
	  this.RightArm.rotationPointX = 0.0F - f14 - 1.0F + f15;
	  this.RightArm2.rotationPointX = this.RightArm.rotationPointX;
	  
	  this.LeftArm.rotationPointX = f14 + 1.0F - f15;
	  this.LeftArm2.rotationPointX = this.LeftArm.rotationPointX;
	  
	  this.RightLeg.rotationPointX = 0.0F - f14 - 1.0F + f15;
	  this.RightLeg2.rotationPointX = this.RightLeg.rotationPointX;
	  
	  this.LeftLeg.rotationPointX = f14 + 1.0F - f15;
	  this.LeftLeg2.rotationPointX = this.LeftLeg.rotationPointX;
	  
	  this.RightArm.rotateAngleY += this.Body.rotateAngleY;
	  this.RightArm2.rotateAngleY += this.RightArm.rotateAngleY;
	  
	  this.LeftArm.rotateAngleY += this.Body.rotateAngleY;
	  this.LeftArm2.rotateAngleY = this.LeftArm.rotateAngleY;
	  
	  this.LeftArm.rotateAngleX += this.Body.rotateAngleY;
	  this.LeftArm2.rotateAngleY = this.LeftArm.rotateAngleY;
	  
	  this.RightArm.rotationPointY = 8.0F;
	  this.RightArm.rotationPointY = this.RightArm2.rotationPointY;
	  
	  this.LeftArm.rotationPointY = this.RightArm.rotationPointY;
	  this.LeftArm2.rotationPointY = this.RightArm2.rotationPointY;
	  
	  this.RightLeg.rotationPointY = 4.0F;
	  this.RightLeg2.rotationPointY = this.RightLeg.rotationPointY;
	  
	  this.LeftLeg.rotationPointY = this.LeftLeg.rotationPointY;
	  this.LeftLeg2.rotationPointY = this.LeftLeg2.rotationPointY;
  }
  
  public void setMainRLegs(float f, float f1) {
	  float RightArmRotateAngleX, LeftArmRotateAngleX;
	  float RightLegRotateAngleX, LeftLegRotateAngleX;
	  
	  
	  if (!this.isFlying || !this.isPegasus) {
		  RightArmRotateAngleX = MathHelper.sin(f * 0.6662F + 3.141593F) * 0.6F * f1;
		  LeftArmRotateAngleX = MathHelper.sin(f * 0.6662F) * 0.6F * f1;
		  RightLegRotateAngleX = MathHelper.sin(f * 0.6662F) * 0.3F * f1;
		  LeftLegRotateAngleX = MathHelper.sin(f * 0.6662F + 3.141593F) * 0.3F * f1;
			  
		  this.RightArm.rotateAngleY = 0.0F;
		  this.LeftArm.rotateAngleY = 0.0F;
		  this.RightLeg.rotateAngleY = 0.0F;
		  this.LeftLeg.rotateAngleY = 0.0F;
		  
		  this.RightArm2.rotateAngleY = 0.0F;
		  this.LeftArm2.rotateAngleY = 0.0F;
		  this.RightLeg2.rotateAngleY = 0.0F;
		  this.LeftLeg2.rotateAngleY = 0.0F;
		      
	  } else {
		  if (f1 < 0.9999F) {
			  this.rainboom = false;
			        
			  RightArmRotateAngleX = MathHelper.cos(0.0F - f1 * 0.5F);
			  LeftArmRotateAngleX = MathHelper.cos(0.0F - f1 * 0.5F);
			  RightLegRotateAngleX = MathHelper.cos(f1 * 0.5F);
			  LeftLegRotateAngleX = MathHelper.cos(f1 * 0.5F);
		  } else {
			  this.rainboom=true;
		        
			  RightArmRotateAngleX = 4.712F;
			  LeftArmRotateAngleX = RightArmRotateAngleX;
			  RightLegRotateAngleX = 1.571F;
			  LeftLegRotateAngleX = RightLegRotateAngleX;
		  }
		      
		  this.RightArm.rotateAngleY = 0.2F;
		  this.LeftArm.rotateAngleY = -0.2F;
		  this.RightLeg.rotateAngleY = this.LeftArm.rotateAngleY;
		  this.LeftLeg.rotateAngleY = this.RightArm.rotateAngleY;
		  
		  this.RightArm2.rotateAngleY = this.RightArm.rotateAngleY;
		  this.LeftArm2.rotateAngleY = this.LeftArm.rotateAngleY;
		  this.RightLeg2.rotateAngleY = this.LeftArm2.rotateAngleY;
		  this.LeftLeg2.rotateAngleY = this.RightArm2.rotateAngleY;
	  }
	  
	  if (this.isSleeping) {
		  RightArmRotateAngleX = 4.712F;
		  LeftArmRotateAngleX = RightArmRotateAngleX;
		  RightLegRotateAngleX = 1.571F;
		  LeftLegRotateAngleX = RightLegRotateAngleX;
	  }
		
		this.RightArm.rotateAngleX = RightArmRotateAngleX;
		this.LeftArm.rotateAngleX = LeftArmRotateAngleX;
		this.RightLeg.rotateAngleX = RightLegRotateAngleX;
		this.LeftLeg.rotateAngleX = LeftLegRotateAngleX;
	  
		this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
		this.LeftArm2.rotateAngleX = this.LeftArm.rotateAngleX;
		this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
		this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
	  
		this.RightArm.rotateAngleZ = 0.0F;
		this.LeftArm.rotateAngleZ = 0.0F;
		
		this.RightArm2.rotateAngleZ = 0.0F;
		this.LeftArm2.rotateAngleZ = 0.0F;
  }

  
  public void setRFBody(float f5) {
	float rotateAngleY = 0.0F;
	    
	if (f5 > -9990.0F && !this.isUnicorn)
		rotateAngleY = MathHelper.sin(MathHelper.sin(f5) * 3.141593F * 2.0F) * 0.2F;
	    
		this.Body.rotateAngleY = (float)(rotateAngleY * 0.2D);
		this.BodyBack.rotateAngleY = this.Body.rotateAngleY;
  	}


  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
    EntityNPCInterface npc = (EntityNPCInterface)entity;
    this.isRiding = npc.isRiding();
    
    if (this.isSneak && (npc.currentAnimation == EnumAnimation.CRAWLING || npc.currentAnimation == EnumAnimation.LYING))
      this.isSneak = false;
    this.rainboom = false;
    
    setRHead(f3, f4);
    setRFBody(f5);
    setRLegs(f, f1);
    
    if (this.isSneak && !this.isFlying) {
      this.Body.rotateAngleX = 0.4F;
      this.Body.rotationPointY = 7.0F;
      this.Body.rotationPointZ = -4.0F;
      
      this.BodyBack.rotateAngleX = 0.4F;
      this.BodyBack.rotationPointY = 7.0F;
      this.BodyBack.rotationPointZ = -4.0F;

      
      this.RightLeg.rotateAngleX -= 0.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotateAngleX -= 0.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
      
      this.RightArm.rotateAngleX -= 0.4F;
      this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
      
      this.LeftArm.rotateAngleX -= 0.4F;
      this.LeftArm2.rotateAngleX = this.LeftArm.rotateAngleX;
      
      this.RightLeg.rotationPointZ = 10.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointZ = 10.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
      
      this.RightLeg.rotationPointY = 7.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointY = 7.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
      
      if (this.isSleeping) {
        this.Head.rotationPointY = 2.0F;
        this.Head.rotationPointZ = -1.0F;
        this.Head.rotationPointX = 1.0F;
      } else {
        this.Head.rotationPointY = 6.0F;
        this.Head.rotationPointZ = -2.0F;
        this.Head.rotationPointX = 0.0F;
      }      
    } else {
      this.Body.rotateAngleX = 0.0F;
      this.Body.rotationPointY = 0.0F;
      this.Body.rotationPointZ = 0.0F;
      
      this.BodyBack.rotateAngleX = 0.0F;
      this.BodyBack.rotationPointY = 0.0F;
      this.BodyBack.rotationPointZ = 0.0F;

      
      this.RightLeg.rotationPointZ = 10.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointZ = 10.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
      
      this.RightLeg.rotationPointY = 8.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointY = 8.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;

      if (this.isSleeping) {
        this.Head.rotationPointY = 2.0F;
        this.Head.rotationPointZ = 1.0F;
        this.Head.rotationPointX = 1.0F;
      } else {
        this.Head.rotationPointY = 0.0F;
        this.Head.rotationPointZ = 0.0F;
        this.Head.rotationPointX = 0.0F;
      }
    }
    
    if (this.isSleeping) {
      this.RightArm.rotationPointZ += 6.0F;
      this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
      
      this.LeftArm.rotationPointZ += 6.0F;
      this.LeftArm2.rotateAngleX = this.LeftArm.rotateAngleX;
      
      this.RightLeg.rotationPointZ -= 8.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointZ -= 8.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
      
      this.RightArm.rotationPointY += 2.0F;
      this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
      
      this.LeftArm.rotationPointY += 2.0F;
      this.LeftArm2.rotateAngleX = this.LeftArm.rotateAngleX;
      
      this.RightLeg.rotationPointY += 2.0F;
      this.RightLeg2.rotateAngleX = this.RightLeg.rotateAngleX;
      
      this.LeftLeg.rotationPointY += 2.0F;
      this.LeftLeg2.rotateAngleX = this.LeftLeg.rotateAngleX;
    }
    
    setRAimedBow(f2);
    
    
  }
  
  public void setRAimedBow(float f2) {
	if (this.aimedBow && !this.isUnicorn) {
	        this.RightArm.rotateAngleZ = 0.0F;
	        this.RightArm2.rotateAngleZ = this.RightArm.rotateAngleZ;
	        
	        this.RightArm.rotateAngleY = -(0.1F - 0.0F * 0.6F) + this.Head.rotateAngleY;
	        this.RightArm2.rotateAngleY = this.RightArm.rotateAngleY;
	        
	        this.RightArm.rotateAngleX = 4.712F + this.Head.rotateAngleX;
	        this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;
	        
	        this.RightArm.rotateAngleX -= 0.0F * 1.2F - 0.0F * 0.4F;
	        this.RightArm2.rotateAngleX = this.RightArm.rotateAngleX;

	        this.RightArm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
	        this.RightArm2.rotateAngleZ = this.RightArm.rotateAngleZ;
	        
	        this.RightArm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
	        this.RightArm2.rotateAngleX = this.RightArm.rotateAngleZ;
	        
	        this.RightArm.rotationPointZ++;
	        this.RightArm2.rotationPointZ++;
	}
  }
*/
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
	  this.Head.rotateAngleX = this.Owner.Head.rotateAngleX;
	  this.Head.rotateAngleY = this.Owner.Head.rotateAngleY;
	  this.Head.rotateAngleZ = this.Owner.Head.rotateAngleZ;
	  
	  this.Body.rotateAngleX = this.Owner.Body.rotateAngleX;
	  this.Body.rotateAngleY = this.Owner.Body.rotateAngleY;
	  this.Body.rotateAngleZ = this.Owner.Body.rotateAngleZ;
	  
	  this.BodyBack.rotateAngleX = this.Owner.Body.rotateAngleX;
	  this.BodyBack.rotateAngleY = this.Owner.Body.rotateAngleY;
	  this.BodyBack.rotateAngleZ = this.Owner.Body.rotateAngleZ;
	  
	  this.LeftArm.rotateAngleX = this.Owner.LeftArm.rotateAngleX;
	  this.LeftArm.rotateAngleY = this.Owner.LeftArm.rotateAngleY;
	  this.LeftArm.rotateAngleZ = this.Owner.LeftArm.rotateAngleZ;
	  
	  this.LeftArm2.rotateAngleX = this.Owner.LeftArm.rotateAngleX;
	  this.LeftArm2.rotateAngleY = this.Owner.LeftArm.rotateAngleY;
	  this.LeftArm2.rotateAngleZ = this.Owner.LeftArm.rotateAngleZ;
	  
	  this.RightArm.rotateAngleX = this.Owner.RightArm.rotateAngleX;
	  this.RightArm.rotateAngleY = this.Owner.RightArm.rotateAngleY;
	  this.RightArm.rotateAngleZ = this.Owner.RightArm.rotateAngleZ;
	  
	  this.RightArm2.rotateAngleX = this.Owner.RightArm.rotateAngleX;
	  this.RightArm2.rotateAngleY = this.Owner.RightArm.rotateAngleY;
	  this.RightArm2.rotateAngleZ = this.Owner.RightArm.rotateAngleZ;
	  
	  this.LeftLeg.rotateAngleX = this.Owner.LeftLeg.rotateAngleX;
	  this.LeftLeg.rotateAngleY = this.Owner.LeftLeg.rotateAngleY;
	  this.LeftLeg.rotateAngleZ = this.Owner.LeftLeg.rotateAngleZ;
	  
	  this.LeftLeg2.rotateAngleX = this.Owner.LeftLeg.rotateAngleX;
	  this.LeftLeg2.rotateAngleY = this.Owner.LeftLeg.rotateAngleY;
	  this.LeftLeg2.rotateAngleZ = this.Owner.LeftLeg.rotateAngleZ;
	  
	  this.RightLeg.rotateAngleX = this.Owner.RightLeg.rotateAngleX;
	  this.RightLeg.rotateAngleY = this.Owner.RightLeg.rotateAngleY;
	  this.RightLeg.rotateAngleZ = this.Owner.RightLeg.rotateAngleZ;
	  
	  this.RightLeg2.rotateAngleX = this.Owner.RightLeg.rotateAngleX;
	  this.RightLeg2.rotateAngleY = this.Owner.RightLeg.rotateAngleY;
	  this.RightLeg2.rotateAngleZ = this.Owner.RightLeg.rotateAngleZ;
	  
	  
	  this.Head.rotationPointX = this.Owner.Head.rotationPointX;
	  this.Head.rotationPointY = this.Owner.Head.rotationPointY;
	  this.Head.rotationPointZ = this.Owner.Head.rotationPointZ;
	  
	  this.Body.rotationPointX = this.Owner.Body.rotationPointX;
	  this.Body.rotationPointY = this.Owner.Body.rotationPointY;
	  this.Body.rotationPointZ = this.Owner.Body.rotationPointZ;
	  
	  this.BodyBack.rotationPointX = this.Owner.Body.rotationPointX;
	  this.BodyBack.rotationPointY = this.Owner.Body.rotationPointY;
	  this.BodyBack.rotationPointZ = this.Owner.Body.rotationPointZ;
	  
	  this.LeftArm.rotationPointX = this.Owner.LeftArm.rotationPointX;
	  this.LeftArm.rotationPointY = this.Owner.LeftArm.rotationPointY;
	  this.LeftArm.rotationPointZ = this.Owner.LeftArm.rotationPointZ;
	  
	  this.LeftArm2.rotationPointX = this.Owner.LeftArm.rotationPointX;
	  this.LeftArm2.rotationPointY = this.Owner.LeftArm.rotationPointY;
	  this.LeftArm2.rotationPointZ = this.Owner.LeftArm.rotationPointZ;
	  
	  this.RightArm.rotationPointX = this.Owner.RightArm.rotationPointX;
	  this.RightArm.rotationPointY = this.Owner.RightArm.rotationPointY;
	  this.RightArm.rotationPointZ = this.Owner.RightArm.rotationPointZ;
	  
	  this.RightArm2.rotationPointX = this.Owner.RightArm.rotationPointX;
	  this.RightArm2.rotationPointY = this.Owner.RightArm.rotationPointY;
	  this.RightArm2.rotationPointZ = this.Owner.RightArm.rotationPointZ;
	  
	  this.LeftLeg.rotationPointX = this.Owner.LeftLeg.rotationPointX;
	  this.LeftLeg.rotationPointY = this.Owner.LeftLeg.rotationPointY;
	  this.LeftLeg.rotationPointZ = this.Owner.LeftLeg.rotationPointZ;
	  
	  this.LeftLeg2.rotationPointX = this.Owner.LeftLeg.rotationPointX;
	  this.LeftLeg2.rotationPointY = this.Owner.LeftLeg.rotationPointY;
	  this.LeftLeg2.rotationPointZ = this.Owner.LeftLeg.rotationPointZ;
	  
	  this.RightLeg.rotationPointX = this.Owner.RightLeg.rotationPointX;
	  this.RightLeg.rotationPointY = this.Owner.RightLeg.rotationPointY;
	  this.RightLeg.rotationPointZ = this.Owner.RightLeg.rotationPointZ;
	  
	  this.RightLeg2.rotationPointX = this.Owner.RightLeg.rotationPointX;
	  this.RightLeg2.rotationPointY = this.Owner.RightLeg.rotationPointY;
	  this.RightLeg2.rotationPointZ = this.Owner.RightLeg.rotationPointZ;
  }

  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
    float scale = f5;
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    this.Head.render(scale);
    
    this.Body.render(scale);
    this.BodyBack.render(scale);
    
    this.LeftArm.render(scale);
    this.RightArm.render(scale);
    
    this.LeftLeg.render(scale);
    this.RightLeg.render(scale);
    
    this.LeftArm2.render(scale);
    this.RightArm2.render(scale);
    
    this.LeftLeg2.render(scale);
    this.RightLeg2.render(scale);
  }
}
